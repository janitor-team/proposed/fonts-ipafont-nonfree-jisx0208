fonts-ipafont-nonfree-jisx0208 (1:00103-7) unstable; urgency=medium

  * debian/control
    - use https for upstream Homepage
    - set Priority: optional since extra is obsoleted
    - set Standards-Version: 4.1.1
  * debian/copyright
    - use https for Upstream-Contact: 

 -- Hideki Yamane <henrich@debian.org>  Sun, 15 Oct 2017 08:44:32 +0900

fonts-ipafont-nonfree-jisx0208 (1:00103-6) unstable; urgency=medium

  * debian/control
    - Build-Depends: debhelper (>= 10)
    - set Standards-Version: 4.0.0
  * debian/compat
    - set 10 
  * drop unused debian/fonts-ipafont-nonfree-jisx0208.lintian-overrides
  * drop unnecessary debian/source/lintian-overrides

 -- Hideki Yamane <henrich@debian.org>  Thu, 27 Jul 2017 23:35:24 +0900

fonts-ipafont-nonfree-jisx0208 (1:00103-5) unstable; urgency=medium

  * fix regression in debian/fonts-ipafont-nonfree-jisx0208.preinst 

 -- Hideki Yamane <henrich@debian.org>  Sun, 03 Jan 2016 16:42:02 +0900

fonts-ipafont-nonfree-jisx0208 (1:00103-4) unstable; urgency=medium

  * debian/control
    - update Vcs-Git 
  * debian/fonts-ipafont-nonfree-jisx0208.preinst
    - fix command-with-path-in-maintainer-script

 -- Hideki Yamane <henrich@debian.org>  Sun, 06 Dec 2015 14:47:08 +0900

fonts-ipafont-nonfree-jisx0208 (1:00103-3) unstable; urgency=medium

  [ Andreas Beckmann ]
  * Use the correct Vcs-* URLs.
  * fonts-ipafont-nonfree-jisx0208.preinst: Cleanup obsolete alternatives from
    ttf-ipafont-jisx0208.  (Closes: #776108)

  [ Hideki Yamane ]
  * debian/control
    - set Standards-Version: 3.9.6 

 -- Hideki Yamane <henrich@debian.org>  Sat, 24 Jan 2015 21:29:59 +0900

fonts-ipafont-nonfree-jisx0208 (1:00103-2) unstable; urgency=low

  * debian/control
    - make it Multi-Arch: foreign
    - set Standards-Version: 3.9.4
    - use git for Vcs-* 
    - drop obsolete ttf- package

 -- Hideki Yamane <henrich@debian.org>  Sun, 16 Jun 2013 03:48:48 +0900

fonts-ipafont-nonfree-jisx0208 (1:00103-1) unstable; urgency=low

  * use XZ for orig.tar, add epoch to version.
  * debian/copyright
    - use copyright format 1.0

 -- Hideki Yamane <henrich@debian.org>  Mon, 25 Mar 2013 17:15:59 +0900

fonts-ipafont-nonfree-jisx0208 (00103-20) unstable; urgency=low

  * debian/control
    - bump up debhelper version to deal with xz option
  * debian/compat
    - set 9

 -- Hideki Yamane <henrich@debian.org>  Tue, 12 Jun 2012 17:43:24 +0900

fonts-ipafont-nonfree-jisx0208 (00103-19) unstable; urgency=low

  * debian/rules
    - compress with xz
  * debian/control
    - add "Pre-Depends: dpkg (>= 1.15.6~)"
    - replace s/Conflicts/Breaks/
    - update "Homepage:" field
  * debian/fonts-ipafont-nonfree-jisx0208.lintian-overrides
    - override "no-upstream-changelog" since they don't provide it.
  * debian/source/lintian-overrides
    - override "debian-watch-file-is-missing" since upstream doesn't provide
      this font anymore

 -- Hideki Yamane <henrich@debian.org>  Mon, 11 Jun 2012 06:32:36 +0900

fonts-ipafont-nonfree-jisx0208 (00103-18) unstable; urgency=low

  * debian/control
    - Fix typo in Vcs-Browser (Closes: #644947)
    - set "Standards-Version: 3.9.3"

 -- Hideki Yamane <henrich@debian.org>  Sat, 31 Mar 2012 20:32:26 +0900

fonts-ipafont-nonfree-jisx0208 (00103-17) unstable; urgency=low

  * rename package
  * debian/control
    - fix Vcs-*
    - update Homepage:
    - Standards-Version: 3.9.2
    - rename package and add transitional package
    - Provides: "fonts-japanese-gothic, fonts-japanese-mincho"
    - set "Conflicts: ttf-ipafont-jisx0208 (<< 00103-17)" and also "Replaces:"
    - fix typo in description (Closes: LP#836596)
  * debian/fonts-ipafont-nonfree-jisx0208.{pre,post}inst,prerm
    - use fonts-japanese-{gothic,mincho}.ttf, instead of 
      ttf-japanese-{gothic,mincho}.ttf
  * remove debian/watch file since upstream don't provide the file anymore.

 -- Hideki Yamane <henrich@debian.org>  Sat, 17 Sep 2011 09:50:22 +0900

ttf-ipafont-jisx0208 (00103-16) unstable; urgency=low

  * debian/watch
    - update to deal with changes in upstream site, again.
  * debian/control
    - set "Standards-Version: 3.9.1"

 -- Hideki Yamane <henrich@debian.org>  Sun, 01 Aug 2010 18:11:05 -0400

ttf-ipafont-jisx0208 (00103-15) unstable; urgency=low

  * debian/watch
    - update to deal with changes in upstream site
  * debian/control
    - update my address and remove DM stuff
    - set "Standards-Version: 3.9.0"

 -- Hideki Yamane <henrich@debian.org>  Mon, 19 Jul 2010 00:16:24 +0900

ttf-ipafont-jisx0208 (00103-14) unstable; urgency=low

  [ Christian Perrier ]
  * Drop x-ttcidfont-conf, fontconfig et al. from Suggests

  [ Hideki Yamane (Debian-JP) ]
  * debian/control
    - set "Standards-Version: 3.8.4"
    - change from "Recommends: ttf-ipafont" to "Suggests: otf-ipafont"
  * debian/watch
    - update to deal with upstream's change 

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Sat, 06 Mar 2010 21:16:41 +0900

ttf-ipafont-jisx0208 (00103-13) unstable; urgency=low

  * use debhelper7
  * switch to source format 3.0 (quilt)
  * drop defoma

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Sat, 12 Dec 2009 17:00:07 +0900

ttf-ipafont-jisx0208 (00103-12) unstable; urgency=low

  * rebuild to build postinst and prerm scripts correctly.

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Thu, 25 Jun 2009 20:10:56 +0900

ttf-ipafont-jisx0208 (00103-11) unstable; urgency=low

  * debian/prerm: remove fc-cache
  * debian/control:
    - set "Standards-Version: 3.8.2" 

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Sat, 20 Jun 2009 16:23:48 +0900

ttf-ipafont-jisx0208 (00103-10) unstable; urgency=low

  * debian/postinst: remove fc-cache 

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Wed, 17 Jun 2009 04:20:00 +0900

ttf-ipafont-jisx0208 (00103-9) unstable; urgency=low

  * debian/ttf-ipafont-jisx0208.defoma-hints:
     - again, set "FontName = IPAX0208*", remove "JIS".

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Sat, 06 Jun 2009 10:59:06 +0900

ttf-ipafont-jisx0208 (00103-8) unstable; urgency=low

  * debian/ttf-ipafont-jisx0208.defoma-hints:
     - set "FontName = IPAJISX0208*"

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Fri, 05 Jun 2009 18:39:24 +0900

ttf-ipafont-jisx0208 (00103-7) unstable; urgency=low

  * debian/ttf-ipafont-jisx0208.defoma-hints:
    - set "Shape = Serif Upright" for IPAMincho and IPAPMincho.
      Thanks to YAMASHITA Junji <ysiijj@gmail.com>       (Closes: #531333)

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Tue, 02 Jun 2009 21:51:24 +0900

ttf-ipafont-jisx0208 (00103-6) unstable; urgency=low

  * debian/ttf-ipafont-jisx0208.defoma-hints: fix pathname. 

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Sun, 31 May 2009 22:09:21 +0900

ttf-ipafont-jisx0208 (00103-5) unstable; urgency=low

  * fix broken alternative symlink. 

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Sun, 17 May 2009 00:49:23 +0900

ttf-ipafont-jisx0208 (00103-4) unstable; urgency=low

  * debian/watch: fix it 

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Thu, 14 May 2009 05:08:10 +0900

ttf-ipafont-jisx0208 (00103-3) unstable; urgency=low

  * debian/ttf-ipafont-jisx0208.defoma-hints
    - set "CIDSupplement = 1"

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Sun, 10 May 2009 09:43:59 +0900

ttf-ipafont-jisx0208 (00103-2) unstable; urgency=low

  * debian/control:
    - new section "fonts" 
    - fix Vcs-Svn and Vcs-Browser fields.

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Sun, 10 May 2009 02:53:48 +0900

ttf-ipafont-jisx0208 (00103-1) unstable; urgency=low

  * New upstream release
  * Upload to Official Debian repository (closes: #471014)
  * debian/control
    - use Build-Depends-Indep: field for defoma, not Build-Depends:. 
      Thanks to lintian.
    - Suggests: xfs (>= 1:1.0.1-5)
    - add "DM-Upload-Allowed: yes"
  * debian/copyright
    - change English license text from gentoo people, thanks!
      see http://bugs.gentoo.org/196340 
  * add alternatives support

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Thu, 12 Mar 2009 13:34:24 +0900

ttf-ipafont-jisx0208 (00101-1) unstable; urgency=low

  * Initial release.

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Sun, 17 Feb 2008 20:09:29 +0900

